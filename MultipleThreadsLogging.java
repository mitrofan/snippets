import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Test {

    public static final int THREAD_COUNT = 10;
    public static final int SLEEP_COEFFICIENT = 0;

    public static void main(String[] args) {
        ThreadGroup group = new ThreadGroup("myThreads");
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < THREAD_COUNT; i++) {
            final MyThread thread = new MyThread(group, "Thread #" + i, SLEEP_COEFFICIENT * (THREAD_COUNT - i));
            threads.add(thread);
            thread.start();
        }

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public static class MyThread extends Thread {
        public static final int ITERATIONS_COUNT = 10;

        private final int initialSleep;

        public MyThread(ThreadGroup group, String name, final int initialSleep) {
            super(group, name);

            this.initialSleep = initialSleep;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(initialSleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < ITERATIONS_COUNT; i++) {
                log.info("Iteration: " + i + ". Time: " + System.nanoTime());
            }
        }
    }
}